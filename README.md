# Simple Carousel (module for Omeka S)
[Video Background] is a module for [Omeka S]. This module provides a basic hero element with a background video

Currently only external videos can be added. 

## TODO
- [ ] Add video upload
- [] Adding alignment configuration options. 

## LICENSE
The module is released under the MIT License.

[Artefacto]: https://artefacto.org.uk
[Omeka S]: https://omeka.org/s
[MIT]: http://opensource.org/licenses/MIT
