<?php
namespace VideoBackground;

return [
    'view_manager' => [
        'template_path_stack' => [
            dirname(__DIR__) . '/view',
        ]
    ],
	'block_layouts' => [
        'factories' => [
            'video background' => Service\BlockLayout\VideoBackgroundFactory::class,
        ],
    ],
    'form_elements' => [
        'invokables' => [
            Form\VideoBackgroundBlockForm::class => Form\VideoBackgroundBlockForm::class,
        ],
    ],
    'DefaultSettings' => [
        'VideoBackgroundBlockForm' => [
            // 'height' => '500px',
            'title' => '',
            'subtitle' => '',
            // 'wrapStyle' => 'overflow-y: hidden;display: flex;flex-direction: column;justify-content: center;',
            'videoURL' => '',
            'calltoaction' => '#more',
            // 'ui_background' => 'rgba(0,0,0,0.1)',
        ]
    ]
];