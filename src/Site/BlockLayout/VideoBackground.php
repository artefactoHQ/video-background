<?php
namespace VideoBackground\Site\BlockLayout;

use Omeka\Api\Representation\SiteRepresentation;
use Omeka\Api\Representation\SitePageRepresentation;
use Omeka\Api\Representation\SitePageBlockRepresentation;
use Omeka\Site\BlockLayout\AbstractBlockLayout;
use Zend\View\Renderer\PhpRenderer;

use Zend\Form\FormElementManager;

use VideoBackground\Form\VideoBackgroundBlockForm;

class VideoBackground extends AbstractBlockLayout
{
	/**
     * @var FormElementManager
     */
    protected $formElementManager;

    /**
     * @var array
     */
	protected $defaultSettings = [];
	
    /**
     * @param FormElementManager $formElementManager
     * @param array $defaultSettings
     */
    public function __construct(FormElementManager $formElementManager, array $defaultSettings)
    {
        $this->formElementManager = $formElementManager;
        $this->defaultSettings = $defaultSettings;
    }

	public function getLabel() {
		return 'VideoBackground';
	}

	public function form(PhpRenderer $view, SiteRepresentation $site,
        SitePageRepresentation $page = null, SitePageBlockRepresentation $block = null
    ) {
		$form = $this->formElementManager->get(VideoBackgroundBlockForm::class);
		$data = $block
			? $block->data() + $this->defaultSettings
			: $this->defaultSettings;
		$form->setData([
			'o:block[__blockIndex__][o:data][title]' => $data['title'],
			'o:block[__blockIndex__][o:data][subtitle]' => $data['subtitle'],
			'o:block[__blockIndex__][o:data][videoURL]' => $data['videoURL'],
			'o:block[__blockIndex__][o:data][calltoaction]' => $data['calltoaction'],
		]);
		$form->prepare();

		$html = '';
		//$html .= $view->blockAttachmentsForm($block);
		$html .= '<a href="#" class="collapse" aria-label="collapse"><h4>' . $view->translate('Options'). '</h4></a>';
		$html .= '<div class="collapsible" style="padding-top:6px;">';
		$html .= $view->formCollection($form);
        $html .= '</div>';
		return $html;
    }

	public function render(PhpRenderer $view, SitePageBlockRepresentation $block)
	{
		$page = $block->page();

		//$urls = [];

		foreach ($page as $pages)
		{
			 
		}
		
		return $view->partial('common/block-layout/video-background', [
			'title' => $block->dataValue('title'),
			'subtitle' => $block->dataValue('subtitle'),
			//'urls' => $urls,
			'cta' => $block->dataValue('cta'),
			'videoURL' => $block->dataValue('videoURL'),
		]);
	}
}
