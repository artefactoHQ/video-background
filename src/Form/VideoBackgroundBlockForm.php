<?php
namespace VideoBackground\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class VideoBackgroundBlockForm extends Form
{
	public function init()
	{
	

		$this->add([
			'name' => 'o:block[__blockIndex__][o:data][title]',
			'type' => Element\Text::class,
            'options' => [
				'label' => 'Title',
				'info' => 'Main heading for the video hero'
			]
		]);

		$this->add([
			'name' => 'o:block[__blockIndex__][o:data][subtitle]',
			'type' => Element\Text::class,
            'options' => [
				'label' => 'Subtitle (option)',
			]
		]);

	
		$this->add([
			'name' => 'o:block[__blockIndex__][o:data][videoURL]',
			'type' => Element\Url::class,
            'options' => [
				'label' => 'Video URL',
				'info' => 'Video source url - please provide the full URL for the mp4 file to use.'
			]
		]);
		

		$this->add([
			'name' => 'o:block[__blockIndex__][o:data][cta]',
			'type' => Element\Text::class,
			'options' => [
				'label' => 'Call to action link (option)',
				'info' => 'Show a call to action button linking to this page'
			]
		]);
	}
}
