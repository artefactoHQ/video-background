<?php
namespace VideoBackground\Service\BlockLayout;

use Interop\Container\ContainerInterface;
use VideoBackground\Site\BlockLayout\VideoBackground;
use Zend\ServiceManager\Factory\FactoryInterface;

class VideoBackgroundFactory implements FactoryInterface
{
	public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
	{
		return new VideoBackground(
			$services->get('FormElementManager'),
			$services->get('Config')['DefaultSettings']['VideoBackgroundBlockForm']
		);
	}
}
?>